/**
 * When an Item looks around, the result may be a simple Item, such as a Rock,
 * however, things get more complicated when items are moving into/out of squares.
 * LookedAt hides some of this complexity.
 */
interface LookedAt {

    fun item() : Item


    fun isOutside() : boolean

    fun isEmpty() : boolean

    fun isMoving() : boolean

    fun isPlayer() : boolean

    fun isDeadly() : boolean

    fun copyable() : boolean


    fun rounded( direction : int ) : boolean

    fun squashable( direction : int ) : boolean

    fun canPush( direction : int, speed : int, strength : int ) : boolean


    fun push(direction : int, speed : int )

    fun collectable() : boolean
}
