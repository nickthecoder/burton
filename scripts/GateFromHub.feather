class GateFromHub : Gate {

    // Not normally set. Used by "Furniture/2x" to force them in sequence 2a, 2b, 2c.
    // Unlike Portcullis, this only allows a single scene name.
    @Attribute
    var requires = ""

    override fun begin() {
        super.begin()
        if ( ! Game.instance.resources.sceneExists( scene ) ) {
            actor.die()
        } else {
            val preferences = Game.instance.preferences.node( "completed" )

            if ( requires == "" || preferences.getBoolean( requires.replace( '/', '.' ), false ) ) {
                
                if ( preferences.getBoolean( scene.replace( '/', '.'), false) ) {
                    actor.event( "completed" ) // Green
                }
                open()

            } else {
                actor.event( "closed" ) // Grey
            }
        }
    }

    override fun activated() {
        super.activated()

        if ( BurtonProducer.instance.previousSceneName == scene ) {

            val player = PlayDirector.instance.player
            player.warpTo( square )
            player.actor.scaleXY = 0.1
            val seconds = 2.0
            val spins = 3.0
            player.replaceAction( null )

            replaceAction(
                // Tumble the player out of the other gate.
                (
                    ScaleTo( player.actor, seconds, 1.0, Eases.easeIn ) and
                    TurnBy( player.actor.direction, seconds, Angle.degrees( 360 * spins ), Eases.easeOut ) and
                    Fade( player.actor.color, seconds, 1.0, Eases.easeOutQuad )
                ) thenOnce this:>postTumble
            )
            
        }
    }

    fun postTumble() {
        val player = PlayDirector.instance.player
        player.replaceAction( player.createAction() )
    }
}
