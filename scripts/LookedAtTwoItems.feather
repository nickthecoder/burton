/**
 * When looking at the contents of a Square, there may be TWO Items sharing the same Square.
 * For example, a Movable Item moving into the Square as well as a static Item which hasn't
 * been "eaten" by the Movable Item yet. Also, it is possible for two Items to share a square
 * for extended periods (e.g. a Repton style nasty sharing a sqaure with Soil).
 *
 * In such cases, in order to keep the game code simple, this is a "compound" of both Items
 * that share the square.
 *
 * Care must be taken when using the result of Square.look(), as it is easy to assume the result
 * is an Item, and forget that it can be a LookedAtTwoItems.
 *
 * Generally, only use methods defined in LookedAt, and do not use "is".
 */
class LookedAtTwoItems( val a : Item, val b : Item ) : LookedAt {

    val actor = a.actor

    override fun item() = a


    override fun isOutside() = false


    override fun isEmpty() = a.isEmpty() && b.isEmpty()


    override fun isMoving() = a.isMoving() || b.isMoving()

    override fun isPlayer() = a.isPlayer() || b.isPlayer()


    override fun isDeadly() = a.isDeadly() || b.isDeadly()

    override fun copyable() = false


    override fun rounded( direction : int ) = a.rounded( direction ) && b.rounded( direction )

    override fun squashable( direction : int ) = a.squashable( direction ) && b.squashable( direction )

    override fun canPush( direction : int, speed : int, strength : int ) =
        a.canPush( direction, speed, strength ) && b.squashable( direction )

    override fun push( direction : int, speed : int ) {
        a.push( direction, speed )
    }

    override fun collectable() = false

    override fun toString() = "LookedAtTwoItems $a and $b"

}
