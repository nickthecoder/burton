// Falls south.
// If hit by a rock, it explodes out the sides.
// i.e. for an upright grenade, it explodes E/W.
//
// Rolls off of curved objects, and in doing to, rotates 90 degrees (which affects the direction of the explosion).
class Grenade : Movable, Explodes {

    static val sideSpeed = 20

    @CostumeAttribute
    var upright = true

    // Used to prevent mutual recusion between adjacent Grenades
    var exploded = false

    override fun rounded( direction : int ) = true

    override fun myTurn() {

        val south = lookSouth()

        if (south.squashable( SOUTH )) {
            moveSouth()

        } else {
            // NOTE. Unlike Rock, there is no special behaviour for balloons.
            if ( south.rounded( NORTH_WEST ) && canRoll(-1) ) {
                moveDelta(-1, 0, sideSpeed )
                upright = ! upright
                actor.direction += Angle.degrees(90)
            } else if ( south.rounded( NORTH_EAST ) && canRoll(1) ) {
                moveDelta(1, 0, sideSpeed )
                upright = ! upright
                actor.direction += Angle.degrees(-90)
            }
        }
    }
    
    fun canRoll( dx : int) : boolean {
        val side = lookDelta( dx, 0, sideSpeed  )
        if (! ( side.squashable( if (dx == 1) EAST else WEST ) ) ) {
            return false
        }
        val below = lookDelta( dx, -1 )
        return below.squashable( SOUTH )
    }

    // Same as a rock
    override fun canPush( direction : int, speed : int, strength : int ) : boolean {
        if (strength < 2) return false
        if (isMoving()) return false
        if ( direction == SOUTH ) return false
        val next = look( direction )
        if ( next.squashable( direction ) ) {
            return true
        }
        return next.canPush( direction, speed, strength - 4 )
    }

    override fun explode() {
        if ( !exploded ) {
            exploded = true
            if ( upright ) {
                explode( EAST )
                explode( WEST )
            } else {
                explode( SOUTH )
                explode( NORTH )
            }
    
            Explosion.create( square )
            actor.die()
        }
    }

    fun explode( direction : int ) {
        if (square == null ) return // Already died!
        Explosion.create( square, direction )

        val item = look( direction, IMMEDIATELY ).item()

        if ( !(item is Empty) && !(item is Outside) && (item.actor != null) ) {
            if ( item is Explodes ) {
                (item as Explodes).explode()
            } else {
                if (! (item is BombProof) ) {
                    if ( item is Valuable ) {
                        val player = PlayDirector.instance.player
                        if ( player != null ) {
                            player.speak( "Oops", 3 )
                            player.actor.event( "oops" )
                        }
                    }
                    item.actor.die()
                }
            }
        }
    }

    override fun copyable() = true

}
