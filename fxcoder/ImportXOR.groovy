/**
 * Generates scene files for the 15 XOR levels.
 * Map Data from  : http://www.danceswithferrets.org/xor/downloads/
 * Solutions from : http://www.danceswithferrets.org/xor/solutions/keypresses.html
 * 
 * NOTE. The solutions are for the Spectrum version, whereas the map data is for
 * the BBC version I believe. There are minor differences in the maps. And therefore
 * I have manually altered some of the maps to make them compatible with the solutions.
 * 
 * See the two sets of images for each map :
 * http://www.danceswithferrets.org/xor/maps/
 * Maps 4, 8 are different.
 */

String genMap( String title, String wall, String text ) {

    def gridSize = 36

    def buffer = new StringBuffer()
    def lines = text.split( '\n' )
    def mapping = [
        "#" : "xorWall",
        "-" : "xorDots",    // Squash E/W
        "|" : "xorWaves",   // Squash N/S
        "@" : "xorMask",    // A collectable mask 
        "M" : "xorMap",     // Reveals 1/4 of the map (we can see the whole map, so just use plain soil).
        "1" : "xorPlayer1", // 1st Player. In my game, the "first" player is currently the top-left most)
        "2" : "xorPlayer2", // 2nd Player.
        "E" : "xorExit",    // Exit
        "!" : "xorFish",    // Looks like a fish. Falls like a rock!
        "<" : "xorChicken", // Like the fish, but "falls" to the LEFT
        "o" : "xorHBomb",   // Explodes East/West falls South
        "x" : "xorVBomb",   // Explodes North/South falls West
        "D" : "xorDoll",    // Pushable, like Burton's Chakir
        "S" : "xorSwitch",  // Turns the lights on and off. Just annoying!
        "+" : "xorBeamMeUp" // Transports to the other "+". Must be zero or two in a scene.
    ]

    buffer.append( """{
  "director": "XORDirector",
  "directorAttributes": [
    { "name": "sceneTitle", "value": "${title}" },
    { "name": "wall",  "value": "${wall}"  }
  ],
  "background": "#000000",
  "showMouse": true,
  "layout": "xor",
  "stages": [
    {
      "name": "main",
      "actors": [""")

    
    def x = 0
    def y = gridSize/2

    boolean first = true

    for ( String line : lines ) {
        x = 0
        for ( String letter : line ) {
            if ( letter == " " ) {
            } else if ( ! mapping.containsKey( letter ) ) {
                println( "Unknown map item : '" + letter + "'" )
            } else {
                def costumeName = mapping[letter]
                if (first) {
                    first = false
                } else {
                    buffer.append( "," )
                }
                def extra = ""
                if (costumeName == "xorExit") extra = """"attributes": [{"name": "scene", "value": "xor/hub" }],"""

                buffer.append( """
        {
          "costume": "${costumeName}",
          ${extra}
          "x": ${x*gridSize},
          "y": ${y*gridSize},
          "direction": 0
        }""")
   
            }
            x ++
        }
        y --
    }


    buffer.append( """
      ]
    }
  ]
}
""")

    return buffer.toString()
}

String genSolution( String name, String next, String cheat ) {
    return """{
  "director": "XORTestDirector",
  "directorAttributes": [
    {
      "name": "cheatInstructions",
      "value": "${cheat}"
    },
    {
      "name": "nextScene",
      "value": "solutions/xor/${next}"
    }
  ],
  "background": "#000000",
  "layout": "xor",
  "include": [
    "xor/${name}"
  ]
}
"""
}


void save( String name, String next, String title, String wall, String data, String cheat ) {

    cheat = cheat.replaceAll( " ", "" ).replaceAll( "\n", "" )
    def baseDir = new File( "/home/nick/projects/burton/scenes" )
    def testDir = new File( "/home/nick/projects/burton/scenes/test/xor" )

    new File( baseDir, "xor/${name}.scene" ).text = genMap( title, wall, data )
    new File( baseDir, "solutions/xor/${name}.scene" ).text = genSolution( name, next, cheat )
}



def level1 = """
################################
#          ---                 #
# ##########-################# #
# #@|   M 2#        -----    # #
#@#@     ### ############### # #
# ###    @                 # # #
#   #@######-##########M## # # #
### # #|                 # # # #
#-- #  @####-###--|##### # # # #
#@| # ##|     @| @ |@  # # - # #
#@| # #@| ############ #@# # # #
#@| # # | #@#@       # ### # # #
#@| #|# | # ###----# # #@# # # #
#|| #M# |          # # # # # #1#
### #|# # # #@## # # # # # # # #
#@# #|# # # ###@@| |   |   | | #
# # # # # #@#@#E@# # # # # # # #
# # # # # ### #### # # # # # # #
#@# # #   #        # # # # # # #
# # # # ### #####-## # # #@# # #
# # # # #@| #       @# # ### # #
#@# # #  @| #-|-###### # #@# # #
# # - # #@| --||#@       # # # #
# ### # #####|--######## # # # #
# #@#|#   -@#--|@#@-     # #@# #
# #@#|###|##############|# ### #
# # #@#@@||                #@  #
# - ####### ############-###   #
# #-                   -@#@    #
# ##-#######---------#####   M-#
#                            |@#
################################
"""

def level2 = """
################################
##                      #@ 2  @#
## !!  #! #--!   #   !  ###  ###
#|--! #@- ##@##-|#  |## # !  #1#
#|@|! !## #@#    ## |@# # #### #
#-@|! -   #   !  #! |M#   @###<#
#@@|- @##@#!!!@!!#@ |@# # #    #
#M############################ #
#     @@@@#@<@<@<#@    M#    # #
##### #####       | #@#<#|<# # #
#!     !-!#  @<<<#@     #|<#|< #
#-     !|@#|<<<<<#| #@# #@<# # #
# #### !#@#  @<<<#<<<<<<#|<# # #
#   !# -#|#@|<<<<#@        #  @#
#   !# @#|###### ###############
#@  @#  -@#@-    #<@|#@#@    @<#
#|  #########  ###@@<#@#@   #<<#
#|      |!#@#< #@#@<   #@#@ #@@#
#|  !   --#    |<#<@<#  |##<#  #
#|  @ # |@###    #@    ###     #
#     # ###@  @< #             #
#| ! !# !!###### #-|#   ##<<<<<#
#| @ @# !@#--@ | #-|#    #@|||<#
#|    # !!# |<|| #|<<##  ##  @ #
#|   !# !!#@ -|< #|<<@#  #@ @<|#
#|#  @#-@!#< |-| #@  @#  #######
#@# @ ##@-#||||- #######       #
#@#   #   #|---- M       #@#|<<#
#@#  !#   ########|<<   #@##<<@#
#@#  @    !  E         #@|<#|<|#
#@#!!!!   -  #  #@#@#@#@       #
################################
"""

def level3 = """
################################
#       #!!!@#     # @# @<#@@@!#
# #  !# #@@@@# #@< # !@|<<#  !<#
# | #@< #@#@<# ### # -#  ## !< #
# |  @@ #@@@<# #!!   #    #@<  #
# | #!# #      #!<             #
# | @@!<########@  # #  !<#  |<#
# |  |<-###2   #@  #@#@@<<#@   #
# |     ##!<##-##### ######### #
# #     #@@  #@#@!! !!!!|<<<@# #
#    !< ###  #@-!!@ ---- |<<<# #
#|###!  #@@  ###!-@ ||@|! |<<# #
#|--#@  ####MMM#@  ---|--  |<#@#
###|### ######M###|######|######
#@  !   #@@@                   #
#@<<< !<########################
#@ !< ! #                      #
#|<<  ! # #########     #<<<<< #
##    @@# #      #@# !  #@   ! #
#    ####   #@!#<#   -!<## @ ! #
# !  @@@#####@<#1#    ! #    ! #
# ###########@!# #  @<<<#@#  ! #
#      !#   #@<#<#@@#@@@#@@#@! #
#@##   @# #      ############# #
#@#     # ######## @<#         #
#@# !<< #            # ! #  ! !#
#@# !!  # #<<<<#   !<# @<#  !@!#
#@# !#  @ #|@#|#  !<|# # # ###!#
#@#@@#@@# #@   #@<<###   #|@@@<#
#@####################<<<#|@E@<#
#@                       #|@@@<#
################################
"""

def level4orig = """
################################
#@##@M#M  # o#@#! @##   ####! M#
#@##  !#  # !!@#-  #   #x|<#M2 #
# -#o1- !<# !o@#x# #o |<#  #o###
# ####  - # --@#################
# #@ #  o## !-## #!#    #@x  !<#
# #  #o #@# o#@#  -# !@ #@|  -##
# # ####### ####@o # o#####  @##
# # #@#@#   #@!##### -#        #
# # !!@ #     -   E########    #
# # -!# # ##########    #@#### #
# # o!@ # #!    !#   o  ###!@# #
# ##### # ##o  |<#  !#  # |x<# #
#   #@# # #x#   !#  !@< #@ ### #
# ! #|# # ##@#  ## |<   #@   # #
#|#   # # ############# ####<# #
#     # # #@            #x   # #
#@ # @# # ############# #  # # #
##x#x##@#  #  |#      ###  #<# #
##########   !<#@ #<o@#x     # #
####|##@#@   ! #  # !@######## #
#     # #  @<< #    !@# #x     #
#       # ####### ##### #  #<# #
#     ! #               #  # #@#
#------ ################# !!!###
#xxxx## #@#@##          # -o-  #
#######@#@ @#o  #       # o-@ @#
##@!@######@#-    #<!< #########
#@ -##   @#@#x<     #  #|@<@<!<#
#x      |< ###    ###       #@|#
#@    #@##                #    #
################################
"""

def level4 = """
################################
#@##@M#M  # o#@#! @##   ####! M#
#@##  !#  # !!@#-  #   #x|<#M2 #
# -#o1- !<# !o@#x# #o |<#  #o###
# ####  - # --@#################
# #@ #  o## !-## #!#    #@x  !<#
# #  #o #@# o#@#  -# !@ #@|  -##
# # ####### ####@o # o#####  @##
# # #@#@#   #@!##### -#        #
# # !!@ #     -   E########    #
# # -!# # ##########    #@#### #
# # o!@ # #!    !#   o  ###!@# #
# ##### # ##o  |<#  !#  # |x<# #
#   #@# # #x#   !#  !@< #@ ### #
# ! #|# # ##@#  ## |<   #@   # #
#|#   # # ############# ####<# #
#     # # #@            #x   # #
#@ # @# # ############# #  # # #
##x#x##@#  #  |#      ###  #<# #
##########   !<#@ #<o@#x     # #
####|##@#@   ! #  # !@######## #
#     # #  @<< #    !@# #x     #
#       # ####### ##### #  #<# #
#     ! #               #  # #@#
#------ ################# !!!###
#xxxx## #@#@##          # -o-  #
#######@#@ @#o  #       # o-@ @#
##@!@######@#-    #<!< #########
#@ -##  @##@#x<     #  #|@<@<!<#
#x     |< ####    ###       #@|#
#@   #@##                 #    #
################################
"""

def level5 = """
################################
# !      #@!     #      ##!#@###
# !@#### ##- ##  #    # ##!#!###
# -#!# # ##   ## # !| # #@- - ##
# # -! # #   ##@ # @# # ##-#- 2#
# #@@# # # !|<## #  @ # ## ## ##
# #@@# # # - ##@ #x|< # ####@ ##
#  # # # #@|<@## #@!  # ##### ##
# !# ! # ######  # o ##   ! # ##
# -@#- # #      ## @ #  M#o # ##
# -### # #   ! !!#@| #!   - #M##
# #@ !-#     - -@# ###@# @| #o##
# ##@- # #|###|<##   ###########
#-!@#! # #|@#@  ##       # ! ! #
# -#!# # #############@< # ! o #
#  #@! # #M    ###x   @  # ! - #
#!! #@|# ## ##  ####@#!  # -   #
#-! #@ # ##1 ##!  M#  @  #@#   #
# # !# # ### #@<####!!   ###   #
# # @# # #!! #  ##x --@  ##   ##
#@# #@ # #@-   #######   #   #-#
#    # # ######### #        ## #
## ##! # ###D####  #-o<o   ##  #
#  #@- #   ###### ##@-!-  ##   #
# # !# #       ## #########   ##
# #@-  ## # #x  #  !!  #     #!#
# #### #x |<<   #  #o|<# #!  |@#
#      #E#    ! #   #  #  @#!  #
# ###############      #  o@<  #
# #   #   #@@ #   #o#o##  # #  #
#   #   !  @#   ###@#@##       #
################################
"""

def level6 = """
################################
#     1#      ########@# !#@@@<#
#  !   #    D #x     #|| !##  ##
# |-|| #   D  #< D|< #   !   !<#
#  #   #  D   ##  |  #|| !   !##
# D DD # !  2 #      # # !#|#! #
#x     #@#    #   D| #@# !#|--@#
######x####x#o#    |@### #####@#
#   M### !##############     ###
#!   # #!<# #@#o#  o   #   !< @#
#@!< # #!   ###--- D # #   ! D##
##-! # #@    M# #--# | #   !|!##
# @! # ##     #      |<  #@< @<#
#  -  |    #< !        #########
#########  #@ # ########       #
# |o    #######          ##D   #
#  !  #       # ##     ###M<   #
#  -  #   M<  ###    !<#     ! #
##   ##       ##  # |< ## #x## #
###@##    D|x    ##  D@#@ !    #
# |###    @#####################
# ########### !|<#@  ! #@|-#   #
# !#@@@@@@@#E @<###  o ##<   D #
# @########## |<#@#  - ##      #
# ##<@<# !#!# |#### ## ## ##<D #
#|      @<#@   #!## #  #   #D! #
#-##      # #   @#         #D! #
# @# oD|  --!     |!   ##    --#
# !# # |# #@!    #|x D<#       #
# @# #D#!!####   # #   # #o#   #
# -!   #--     ##@#@#  #@#@#  @#
################################
"""

def level7 = """
################################
##                         #  !#
#@@#        |<< !      # #    -#
#### #### --   DD    ### ### # #
#!!# #@@#o##||    ####     #|< #
#@-# #######@@ @@#!    !@! #@ @#
# ##  !@#   #####M- D M#########
#    D<##              #      @#
# #    @#     ######## #   D  ##
#@## #####|#D #   @ !# #    D ##
###    ###o#  #  D  @  #   D  ##
##  DD  ##@#@ #     ##### #    #
#  DD D  ###### D   E# -! #### #
# DD  DD #    # D 2###< #  | # #
# D  DD  #  D #      #     |@# #
#  D@D   #D   ##########   |<# #
#  DDD  ## D     ! ##@@##    # #
##     ###    1  @ #         # #
#### #####         ## ######## #
#@!#    #######x##### ##   @   #
#|<# |< #     ###      #     ! #
# !#    #     M M      #   ||- #
# @#    #     ### !    # ##    #
# !#    ## ####!########  #    #
# @#    #  #   -   #      #    #
#D!#    #-      #  #      #  D #
# @#    ####       # o    #  D #
#D #    #  |    ######    #  ! #
#  #    #@-o   o@@###   #o#  - #
#@#o- # ###################    #
#@###@#   @#@@@@@@@@      #    #
################################
"""

def level8orig = """
################################
#@@E! #S<<#| !!S#|M#@!@@#!!@#@##
#@@ ! #|<<  #!!@#<<# -   --|<  #
#EEE! #      -- #     !####   1#
#   !    @# #####    o#!#    Dx#
# S ##S####S    ### ##o##   Dx #
# #@#  #@   #S# #   ###o#!!    #
# ### ## ###### # ##############
#     #  #      #   !  # !   ! #
#######S##S!    #!  !@<# - D ! #
#        ###   @#!  !@ #     ! #
# ##  ## -@# o###! ###|#@  # ! #
# #@|<#@ |<# #@##!   @@### @ ! #
# ##  ############    o#<x #o#@#
# ##@## ! #######     #### #####
# ##### -         #x          @#
# !#!## ########################
# -!<## #   |  #||     #      @#
#  ! ## #2 #S  #-#x D  ##<######
#  !  # #  #  !#M#   o<#       #
#  -@## #<<#  S#-#D  # #  ###! #
#  #### #||### #|#  -|<S  #@#@ #
#     M##@             ####@## #
########################@@#  # #
###! @#  !!######   @#    #  # #
#@#-D    --   @@# # ## #### !  #
#@#  ##S###   |<# @ #       ####
#@#     #M#Sx   # # ##-###  # @#
#@#  Do #S#     # #     @#  #  #
#@#   #   ###<  # ##### ##  # ##
#@##  D D #@#   S    @#        #
################################
"""

def level8 = """
################################
#@@E! #S<<#| !!S#|M#@!@@#!!@#@##
#@@ ! #|<<  #!!@#<<# -   --|<  #
#EEE! #      -- #     !####   1#
#   !    @# #####   #o#!#    Dx#
# S ##S####S    ### ##o##   Dx #
# #@#  #@   #S# #   ###o#!!    #
# ### ## ###### # ##############
#     #  #      #   !  # !   ! #
#######S##S!    #!  !@<# - D ! #
#        ###   @#!  !@ #     ! #
# ##  ## -@# o###! ###|#@  # ! #
# #@|<#@ |<# #@##!   @@### @ ! #
# ##  ############    o#<x #o#@#
# ##@## ! #######     #### #####
# ##### -         #x          @#
# !#!## ########################
# -!<## #   |  #||     #      @#
#  ! ## #2 #S  #-#x D  ##<######
#  !  # #  #  !#M#   o<#       #
#  -@## #<<#  S#-#D  # #  ###! #
#  #### #||### #|#  -|<S  #@#@ #
#     M##@             ####@## #
########################@@#  # #
###! @#  !!######   @#    #  # #
#@#-D    --   @@# # ## #### !  #
#@#  ##S###   |<# @ #       ####
#@#     #M#Sx   # # ##-###  # @#
#@# DDo #S#     # #     @#  #  #
#@#   #   ###<  # ##### ##  # ##
#@##  D D #@#   S    @#        #
################################
"""

def level9orig = """
################################
##!##!##  @<|!<##@@<#        |S#
##!#@!## ##x|<###|<<<  ###   !|#
#M@!<<<#       !#|#   ###    !|#
#- -  @#    #  -#    ##@#x!< #|#
#  # ###### # #    D    #@!    #
##1#     |< #    # !!!  # -!   #
########  # #    | ---  #  #   #
#!#!#@ #### #    ### #  ##x##x##
#- @|<  |<  #  #o#@#@##x#@-@@-@#
##          # ######### ########
##!|<    ####       #@         #
# -  -!## o   #@##! #########  #
#@# # S#M|<<  !@#!!   M###@##E #
###x######    @!#@!#   ##@!#####
#@##  !| #### ###@--   ###-!####
#  |< #  #!     ######!# 2 - ###
#        #S ##   !|@# -# # @<<##
#x   !|< ##   !< !@@#    #-@-###
#S#  !   #@x ---!! @##########!#
#@####o#|###   --!@##        !!#
#   !@@#o|<   oM !#    ! o  #@@#
#@@####-- # #######  ##!#!<    #
#@@#              S  # @ @   ###
#### ############ ##############
#             !<# #            #
# #########  @< # # ## !#@#S<# #
#     !   #         !! -#@#### #
#     - |<#  ##!#-- !!# ###!   #
#o#o#   D #  #|@#@# -- @#@ !|<<#
#S#@x  #  !  #<@### ## @#@ - |<#
################################
"""

def level9 = """
################################
##!##!##  @<|!<##@@<#        |S#
##!#@!## ##x|<###|<<<  ###   !|#
#M@!<<<#       !#|#   ###    !|#
#- -  @#    #  -#    ##@#x!< #|#
#  # ###### # #    D    #@!    #
##1#     |< #    # !!!  # -!   #
########  # #    | ---  #  #   #
#!#!#@ #### #    ### #  ##x##x##
#- @|<  |<  #  #o#@#@##x#@-@@-@#
##     #    # ######### ########
##!|<    ####       #@         #
# -  -!### o  #@##! #########  #
#@# # S#M@|<< !@#!!   M###@##E #
###x######    @!#@!#   ##@!#####
#@##  !| #### ###@--   ###-!####
#  |< #  #!     ######!# 2 - ###
#        #S ##   !|@# -# # @<<##
#x   !|< ##   !< !@@#    #-@-###
#S#  !   #@x ---!! @##########!#
#@####o#|###   --!@##        !!#
#   !@@#o|<   oM !#    ! o  #@@#
#@@####-- # #######  ##!#!<    #
#@@#              S  # @ @   ###
#### ############ ##############
#             !<# #            #
# #########  @< # # ## !#@#S<# #
#     !   #         !! -#@#### #
#     - |<#  ##!#-- !!# ###!   #
#o#o#   D #  #|@#@# -- @#@ !|<<#
#S#@x  #  !  #<@### ## @#@ - |<#
################################
"""

def level10orig = """
################################
#          #!#|   |#!#         #
# ########  -  # #  -  ## |<<# #
#        ####### ########|<<<# #
# #<<<<##  @@@#   !     #|<@## #
# ## + #  #####   #     ####   #
# #!  #  #         #    #@   ! #
# #@ # + ## !           #D @x# #
# #@@  # !#D#     S<<<< #@ o   #
# ##@@ - @# #   o @<<<<x###### #
# #######################-@<<# #
# #     !<#@#!    1## ###  ! # #
# # ##< -@#@#- !   ## !##  - # #
# # ##@ @<# #@!-   ## -####  # #
# # ##<   # # -  !<##  ##!#  # #
# # ##   |< ## |<@ ##@E##@#  # #
# # ##      !    |<######@M#   #
# # ########################## #
# # #@   !M#!@!## @<  o #D|x<# #
# # #   |<#!<<<#@-    - #o  |# #
# # #@<<##!<    #    !< #D @<# #
# # #@<<@<<@ #o#@|<<<<  #@   # #
# # # ## ######## ### ###### # #
# #                            #
# ##############################
#                         !   2#
########M##########M#####!<x  !#
#@ !@            !<     #-!  |<#
## @< #|<##!|< !#!  DD !# # |< #
#@    #|<#@-|#|<#! @  |<#  !<  #
#@  @@#-|@#@#@   -   |<@#@|< #@#
################################
"""

def level10 = """
################################
#          #!#|   |#!#         #
# ########  -  # #  -  ## |<<# #
#        ####### ########|<<<# #
# #<<<<##  @@@#   !     #|<@## #
# ## + #  #####   #     ####   #
# #!  #  #         #    #@   ! #
# #@ # + ## !           #D @x# #
# #@@  # !#D#     S<<<< #@ o   #
# ##@@ - @# #   o @<<<<x###### #
# #######################-@<<# #
# #     !<#@#!    1##S###  ! # #
# # ##< -@#@#- !   ## !##  - # #
# # ##@ @<# #@!-   ## -####  # #
# # ##<   # # -  !<##  ##!#  # #
# # ##   |< ## |<@ ##@E##@#  # #
# # ##      !    |<######@M#   #
# # ########################## #
# # #@   !M#!@!## @<  o #D|x<# #
# # #   |<#!<<<#@-    - #o  |# #
# # #@<<##!<    #    !< #D @<# #
# # #@<<@<<@ #o#@|<<<<  #@   # #
# # # ## ######## ### ###### # #
# #                            #
# ##############################
#                         !   2#
########M##########M#####!<x  !#
#@ !@            !<     #-!  |<#
## @< #|<##!|< !#!  DD !# # |< #
#@    #|<#@-|#|<#! @  |<#  !<  #
#@  @@#-|@#@#@   -   |<@#@|< #@#
################################
"""

def level11 = """
################################
#@  #  #-!  ||!#@ !   ! #  o  !#
#  # | #@< !-|@#x - |<# # |<<<<#
# ##!|  #  @!-o#x   |<# #    @ #
# #+!x< #@#|<|@#@#  |M# #  2 Dx#
# ##!   ### ###### ###  # D   |#
#  #@ #                ##    @ #
#   ###################!########
# @ ##<<<<<<<<<<!<<<<<<<< D  # #
# | #           -            # #
# | #|D                      # #
# |!#    ##||||||||####      # #
# |@#   ####|||x||##@@##x<   # #
#|| ##o##@@##----#@@@@@#   D # #
# | #@## |< ##@@@@@#####  #### #
# | ###  D D #@@@##   ###      #
#|#|         #####! !    #  !|<#
##@#o    D @###! @< !  ! # -#  #
#####   D  #@ @<    # |x     o #
# !o#@@   ##        #  # #######
# @@########## o###x<|<#@#  !  #
#   o  #@!#@@#@#@<#@@M<#@# ##  #
# D DD # M#@@#@#|<########  #|<#
# D @S #  ######|<##   !#   # ##
########          #x !@!## ##|<#
#!!!!x #S   o|   !#S #x@#@ E#  #
#----  ####### D !#x    #####  #
#    D #@x   #D@ @#####        #
#1D    # D @<#########| ##  Dx #
#   @< #  D!+      @@@@@###    #
#M     #   -@#     @@S@@##@#  @#
################################
"""

def level12 = """
################################
# o @##         # @#@     ##@! #
# -! #  #  o  ! #|x#  ### #@<< #
#  D    ! ##@@#    #<  ##     @#
##    ### ##o##x       ##### ###
#@   o# #  #######o###o#####   #
##@ ### #      @@###|#####!#@# #
##########  ||<###@|<<@#|<@##! #
#x#o#o#o#  #x  |#@#   ##    #- #
#########< #@@#<     ##@@#     #
#x1#E###|  #x@#  #  |<#### ##|<#
########|  # ##< # !<#! !# #@|<#
#MMMS###|        #@<|#! @# ##@|#
########  #!   #######!< # #####
########@#-!     #####@  # #+! #
##@--@#####- @<# #@        ##- #
#@@###x   #@#### #   ! ## @#   #
# ##@## # ###    #   ##o# !# # #
#  #@#  #     #### ##  -# @# ! #
#@ #   ##   ###@@#        ## ! #
## ###|<##  @##### D2! #@    #-#
#@   #@#@#   #@@@@   ! ##### ! #
#x !< ##@##  ############  ##! #
#@ -|@@#! #   ###@@  !<#   ### #
########- ##<   ###+ !   #  ## #
#x####### # D     #  -  ###  # #
########@ #### ## #@#   ###    #
#@@@@@##@ ##@< ## ### ####  o  #
#@   @###  #   ##     #@#   #< #
#@  M@####   ###|<< #- #  D D  #
#@@@@@####S<###@@   ##         #
################################
"""

def level13 = """
################################
#                   ##M  ! ! !M#
# ################   ##@ o o - #
# #@@x<#      #    o  #  - -@|<#
# #@@@@# ! !  # -#|- |<##@ #####
# ####@# --@- #  |||  |<##     #
#   !# #      ## ||| ###### ##<#
#   !# #      #  --# #@ ! # #|<#
# # -  #    |x#@     ## !<#   |#
################### @#  @@# #-@#
#  2   # #    #@  ##### ### ####
#    ! #@!  ! #  ! ##M  #      #
# o  - #@!  # #  -  ## ##  !   #
# #x#< #@!    #|     # #  ##   #
#    @ ###   o#@  o<<# #  ###| #
#      #   @x### |-  # #  #!|x<#
######## #####M###- ## #  #-   #
#      #-      # ! |<# #  ##!  #
#    ! # !|<< ## o | # #  # !  #
#| #<-## @@   #  @x| # #    @  #
#|  |< # @    #  ! | # #       #
#   @  #    ! #  # # #         #
#x     # @@x#o#      ##   @#@  #
#|#####################   ||<  #
#### ###@ !     ! ! ! ##   !   #
# !<  ##|<<###! @ @ @  ## -- #<#
# -@  ##@    @o       |<###    #
#  |  ##x |   -          ##   1#
#    |x#@  #  #           ######
######################### ###@##
#                         ##E  #
################################
"""

def level14 = """
################################
#                   #@|< #@#@! #
# #|########|<#####  #   # ##! #
# #|--!|!-##@##@ !## !<#@# !#@<#
# #|!|<|D ###    @<# -#### -#  #
# #|-!--|@#   o  ### @#!    #  #
# ##|-|-|@#  !!!   ####-    ## #
# @######### --#      #      # #
# #@!###@@@  -|#<       #### # #
# #--|<#######@#   -D--  #     #
# #   ##     #@#   ! !#  ##!   #
#   |<#! ! |<#@--! #@##  #@! ###
#    ##!#@  -##########  ##x   #
#  D # !#|   ## !@!  ##  #---! #
#  #@# !< -| ##|<#-! #   #@o@< #
# !<##!@!+@ ### DDD# #    ##   #
# !## -|< ###@#      ##-   #####
# !#      #@#@##    ##   o     #
# -# ##o#-  #|<# ##### -###M ###
# @#E#@###### ## ###!#   !#  #@#
# #######  !#    #1@<##  @#  #!#
# #@! #    @# ####M |<#   ## #-#
# ##- #! !< # ###### ## @|<  #|#
# #@  +- @ #x    ### #       #|#
# ### @#  !## ##|<##   ####### #
# # !#### -@#    ### #  #@ |   #
# #!< !@##### ####2M ## #x!<<  #
#  @  @<#     ######### # @   ##
##### |<#####           #|x< | #
#@###  ##@@@############# D  @<#
#                            M@#
################################
"""

def level15 = """
################################
#          #####     #         #
# ### #x ! ####   !! #     1 ! #
#  !  #  -###     @# #      2D #
###-  #<   #         #         #
##+o     D###o###### #x#########
####   #  -#o#    E# -#       @#
################################
###  ! #                       #
###D - #  #####         #####  #
# +    #     ##   ####  ##     #
# # #<!#    ##          ####   #
# # #|x#   ##     ####  ##     #
# #    #  ##            ##     #
# ######  #####         #####  #
# ######                       #
#S###!## ##  ##          ####  #
#   |< # ##  ##   ####    ##   #
#      # ######           ##   #
#  #   # ##  ##   ####    ##   #
# ##o! # ##  ##           ##   #
# !#-- # ##  ##          ####  #
# ######                       #
#      # #####          ##  ## #
#- #o  # ##  ##   ####  ##  ## #
#   !  # #####          ##  ## #
#x  # -# ##  ##   ####  ##  ## #
###    # ##  ##         ##  ## #
#S#    # #####           ##### #
########                       #
# MMMM##                       #
################################
"""

def cheat1 = """
x
DDDDDDDDDDDDDDLLUDDLLRRDRRLDRLLLLLLLLLLLLLLLLLLLLLLLLLLLLLUUUUUUUUUUUUUUUDDDDDDDDDDDDRRUUUDDDLL
DDDRRRRRRRRRRRRRRRRRRRRRRRRRRRRRUUUUUUUUUUUUUUUUUUUUUUUUUUUUULLLLLLLLLLLLLLLLLLLLLLLLLLLLLDDDDD
RRDDDULLDDDDDUUUUURRDDDDDDDDDDDDDDRRDDDDUUUUUUUUUUUUUUUUUUUUUDDDRRUDLLUUUULLUDRRRRURRLDDRRRRRRR
RRRRRRRRDDLLLLLLRDDLLRRUURRRRRRDDDUUULLRUURRRDDDDRRDDDDDDDDDDDDDDDUUUUUUUUUUUUUUULLDDDDDDDDDDUU
UULLUUUDDDDDDDDDDDDDDRRUUUUUDDDDDLLLLLLLLLLLLLLLDDRRRRRRRRRRRRRLLLLLLLLLLLLLUURRRRRRRRRRRRRUULL
LLLLRRRRRRUULLLLLLLRRRRRUUUUUUUUUUUDDDDUUUUUULLRRDDDDDDLLUUUULLLLLLLRRRRRRRDDDDDDDDDLLLLLLDLRDL
DRRDRLLULULLUUUURRUUDDRRRRRUUUUULLDDDUUDLRUULLLDULLLRDDDUUUUUDDLLDDDDDLLDDDDDDRRDDLLRRUURRLLLLU
UURRDUUDLLUUUUUUUUUUUDDDDDDDDRRUUUUUUUUURRRRRLLLLLDDDDRRRRRRRDDDL
"""

def cheat2 = """
x
DDDDDDDDDDLLUUUUULLLDDDDDLLLLLLLRUUUUDLRRRDUUURRLLDDDULLLUUDLLLUDLLUDLLUDDDDDUUUDDRDUUURDDURRRD
DDDLLLLLRRRDDDDLULLUDRRDDLLRRRURUDRUUDDDDDDDDDDLLLLUUULRUDURUDLDDDDRRRRRRRRRRRRURRDURRRRLLDURRD
DDDLLLLLLLRUDRUUDDRRRRRUUUULLLLLLUUUULLUURRRRRRRRLDURDDDLLLDLRUR DULULRDRRRUUUUUULDDLLUULUULLDD
DLLUUUDDLLULUDLDURDDDLDRDLRDLDDUURDRDDRUDLLLRRUULUUURRRRRRRURRLUUUURRDDRDDDLLLLLLLDDDDDRDDLLDDL
DULLDULLDULLL
x
LLRRRRRLLLDDLLDDRLLLUUUULLRRDLLLDDRDDUULLDDLRUUUUULLLDDDLLLDLLUDRRRDUURRUUULLLRDL
LDUULLLDDDDDUUULLRRDDLLLLDRLUUULLD LDDLDDRRRRRRRRLLLLDDLLRRRDDDDDRUDRRUUUDDDLLLUURLUUURRDRLLLLL
LLLRLDDRRDDRLLDDDDDDULDDDDDDDDDDUUUUUUUUUUUUUUURDDDDDDDDDRDDDDDRRLR RRRRLLLLLUUDDRRRRDRRRRULLLL
LLLLRRRRUUUULUURURLLUUUULLDDLLLDDDDRDDDDDRRRRRRLLLLLUUUURLUUULRRLUUURURRRDRLLDDDDDRDDDDRDRRRR
"""

def cheat3 = """
x
UULLLDDDDDLLULLDDDRRDURRRDDDLLLRUDRRUUURRRRRUDRDLULLLDDDUUULLLLLLLDDLLLDRUUUUULDDDUULLLDDDDRLUU
URUUULLLUUURRRDRRRLLLUURUUUURRUU UUULLLDLLLDDRRDDLLDDUDRURUUULLUURRRURUUDUUDLUDRUUUUDRRDDLLDLRR
RDDDDDLLRRDDLLDDDDRRLLLLLDLDDDDDDDDDDDRRRRRRRRRRRRRRRRRRRRRRRUUUULRUURRRRRRUUULLDUULLLDDRLUURR
UULLRRRRRUULLLLLLLLLLLRDDDDLLUUDDDDRUURDUUURRRDDLLLRRRDDLLRUUUUUURRRRRRRRDDDDDDDDLLLLDDUURRRDDU
ULLLDDDDDDRRLUURRDDUxDDRDDLLLRRRUULDULLRRRDDDRRDDLLLLLRRRRRRRRRRLUULLRRULRULULDLLUDRRRRDDUDRRR
URDRRLURULLLLLRRRRRUDURRRRLLLLLLLLUU UURRRDDUURRDULLLDDUURRRRRRDDLRRRUDDDDDDUUUUUUULLLUDRRRUUUU
LLLRRDDDDLLLLLLLLLLURURLUURDRLLDDDRRRUUUDRUUDDDDLLLLLULDDDLLUDRRUUUUU ULLDULLDDDDLLLLULUURRLURR
DLLRRLDDxL
"""

def cheat4 = """
x
UULRDDRRDRRDDUUUUULLRDDDDLDLLLLUDDDDDDURLRURUDRRUDDDLRDDDDDDDUUUUUUUUULLLLUUURRRUUULLLLLUUDDDDD
DDDDDDDDDDDUUURLDDRRRRDUUUUDDDDLD DDDDRRRDLLRRUUUDDDDDDUUULLLRRRDLLLLRLLDDDDLRDDLRUURLUURDRLDDR
RRDUURRLDUDRRDRRRRRRRRRLUURRUDLLLLDLURUUURRDURRRRRDDDLUUULLLLLLLRRR DDDLLUULLLLLLUDRRUDDDUURRRR
URRRRRRRRDLDDDDRRRURRDRRRLLLULLUDRRUDLLLUDRRRDRRRULRULLLDLLLLLLxLRRUDLLDLLLUDULULLDDUDLLLLUULDL
RLDDDD DRURLDRRRUURRRDLULLDRLDDRLUUURRRDLLDDRRRRRRRRLUURLLLLUDRRDDRRRDDDDDDDDDDDDDDDUULLLLDLDDD
RRRRRDULLDUUUUURRUUUUUUUUUUUUULULDRRDDDDDDDDDDDDDLLLLLUULRRRRUUULLLUUURDRRDULLLUUDRUDURURLDLLU
UDDDDDDDDDLLDDDDLLLLLLLLLLLLLLUUUDDDRRRRRRRRRRRRRRUUUUUUULLLLLLLLLLLLRRRRRRRRRRRRUUUUUUDDDDLUL
LRRRUUUDDDLLLLUURURRDLRRDDLLLLUURRRDDLLLLLLLLLUULRRDDRUULLLLDDDRLUUURRRRDDRRRRRRRRDDDDDDDDDDLLL
LLLUUUULURRRR RDDDLLLLUUURRRRDDDLLLLLUUURRRRRDDDLLLLUUURRDDDLLLLLUUULRDLLLDDUULULUUUUUUUUUURRDR
RUDRRLLLLLRUUUUUUUUDDDDRLURRRUUUDDDDDULLLDDDDRRRRRRR
"""


def cheat5 = """
x
RDDDRRUUDDLLLLRURUULUULRRRRDRDRRRLxLDDDDDDUUUUUUULLRRDLULLLRRRUUDDRDDDLRDDDDDLLUUULLRRDDLLDURRD
LLULULDDLRUUUUUUUUUULLDDDDDDLDDDU ULRLRDDULDLRUUULRUDUURLULDDDDDDDDDRRRRRRDDDDDDDDDDLRUUUUUUUUU
ULDDDDLLRUUUDLLDDRRRDDLRUULDRRDDDDDLLLLRRRRUURRRURUURRUUUUUDLRLLDDL LDUUUURRDDDDDDDRRDDDLLDDLLL
LDDDDDRRRRLUUDULULRURRDRURLxRDDDDDLDDDDDDRRRRRULUDUULLDRRDDDDULULDDULLLLDLLULLDLLULLDLLULLDLLUU
URRRRR UULLLURURRUUUUUUDDDLRUUUUDLURUUDDLULRRUUULLULRRRUULRUUDLLUUULDRDDRRDDDDDDDDDDDDDDDDDDDLL
LLLUUUURUULUDRRUURLUUUDDULRLUULURLRURLLU UUUDRLURRLUULUUUUUDRLUURRDURRRRRDDDDDDDDDDRRRRRRDDUURL
DDLRURURLUUURUUUDLRUULRUUULLLLDDDLLDDDUUURRDDUDULRDLDURDRLUUUUULRULLRRRRRR DDDDDDDLDDLLLDLLDDRL
UULLDDDDDDDDDDDDDRRRRRRDDLDLLUUURRRDDLUULLLULLRDDURRRRRDDLDLLLUDLL
"""

def cheat6 = """
DDDDDLUDLURRUUULDULDLRULLDRRRRDDDDDDDDDDLLLRLRULLRULRLULRRRUUDDDDDRRRRRRURRRDDLUULLDLUULRRRRRDD
RRRRRRRUUULxRUUUULDLDLDLLDDURRRRR DDRRURRDRULLUUUURRRDDDRDDULLLLULDDLLLLDDDDDRRDDURRRRRRRUUUULL
RRDDDDRRUUUDDRRURRDUUURRRLDDDRLUUURRLLLLLULDDDDUUURRRRRRxRDDDRRUUUU LLLLLLLLLRRRRRRRRRDDDRRURRU
URRDDRUxLLLLLULxLULLLDDLxDUUUUUULLDDDUUUUUDDRRRRLRxRUUULUUUUxLLRRRDDDRRRDULLRRULUULUULRDRUDDLLL
LLxDDD DRDDDLDLLDLLLLLLLLRDDRRRDDLDLDLLLUUULLLLDDDDRULLLUxDDDDDRDDDLDLLDLLLLLLLDDRRRDDLDLDLLLUU
ULLLLLLRRRRRDDLLDDLLxUURDDUURRRRDDLDUxLU URRDDLRRUUxRUULDDUULLDDDDLLLDDLLRRRRRRLLLLUUURURRURLLL
UULLDDLDLDULUUUDDDUUDxLLLUUULLLDDDUULRULLLRRRDDLRUULLLDRRDDDLDLDDDDDUUDDUU RLDDRRRRUDLLLLUUUUUR
UURUURRURRDDRRRRRRDDRRRURURRRUURRRLLRRRURRRRRRLLDDDLLLDDLRUURRRURRDDDLLLLLUURUDRUURRRDDLLLDULLD
DDDLRRDDLURRR URRDLLLLDLDDDDDLxUURRRRRDDRRRRRRDDRRRURURUURRDDDDUUURRUDDDDUUUURRURRRRDDDLDDLLDDD
DDDDDRDRRRRxRRRRDUURRUUUULLLLLDDDDDRRRLxLxLLUUU UURRRRRDDDDLxRUUUUUULLLLLDDDDDDRRxLLLDDDURRRRDU
LLLLLLDUURUULLLLLLLRRLRLLDDUDURRULUUUUUDDDDDRRURUUUULRDDLLLDDDRDLLDDDLRRLLRUUURRR UxLLUULLLDLLD
DRUxLDDDRRUDLLUUURUxUxRUUUULLLDDDDDDRDRRUDDLLUULUUUUURRRDDDLxRUUDDLLLULRDRDRxDLLLDxRULULLUUUUUL
RDDxDRURRxUURRRDDLLU UxRUUUULLLLLRRRRRDDDDDLLLLDDUUULLLURLLLDDRDDLLLLRRRRUUULLLLRDULLLULLDULLDR
UURRDDLLRRRRRLRRRRDLULLLLUUDLLxRRDDDDDDDLLLLULLLLLLLLL LRRRUURRDDLLLLLLLLLDDDLLLURLUURLDDDRRRUU
UUURRRRDRDRRRRUUUUUUDDDDRUUUxDRRRRRRRUUUUUL
"""

def cheat7 = """
xLUUUULDULLDDRRRDDDLUUULDLDRRDRRRRRRUULLRDDLLDDDLRRRRLDRRRURUUUUURDRRRUUULLLUDRRRUULLDLDRRRDDRRU
URLLLDUUUUDDDDDUUUUUUUUURRRRDDLUU LDRDLUULLDDDDDDDDRDDLLLLULDDDDRDLUUUURRRRUUUUUUUUULDRRRRDLLUL
DDDDDDDRDLLLLULDDDDRDLLLLLLLLLLLDLLRDDDLRRRDDDLULLDRRRRUUULLLUUURUU LDDDDLDRRRRURRRRLLLDDDDRRRL
LULLLUULLUUUURRRRRRRRRRRUUUUURDRRRUUUUUUUURRRUURRLDDDDRDDDDDDDDDDDDDDDLLDLURRURUUULRUULRDDLRULU
LLDURR DLLRRDDDLLUUDDDRRUUUULLRULLLULDRRRDLLLDDDDLDRUUUUURRRRRDLDLURUULLLULDDDDDDLLDDDRRRULRUUL
LLLLDRUURRRRDDLUULDDULLDRRRDDLLRRRRRRLLL LLLUUULLURRRRUUDRDDDDLDRRRUUURRRDDLLLDDDLLLLLLLLLLLLLL
LRRRRRRRRRRRRRRRRLUUUUUUUUUUUxDLLLUUDDRLURRURUULDDDDRUURUULDDDLLULURRUURRD RRRLLRRUUULLLLLLLLLD
DDLURUULRRRRRRRRRUULDRDLLLLLLLLLLDLLUDRRUULDLDRRRURRRRRRRRUURRURRDLLURRRRDLLLLDDLLLLLLLLLRRRRRR
UURRRRRRLLLDD LLLLLLLLLRRRRRRULLRRDLLLLLLLLRRDLLLDDDUUURRRDDLLLUUURRRDDDLDUUUURRRRRRUURRRRRURUU
URRDRRRRLLDDDRRLURUULLLLULLLLLLLLLLLLLLLLLLLLLL LLDLRURRDDDDDDLLLDDUUUURLDDDURRRDRRRLLLUURDURRL
LLDDDDRRDRDRDDLULRDDLULLDDDDURRUULULDDDLLUULUUURURURRDDDDRDLULDRRRUUULULDDDRDLUUD LDRUUUUULLDRR
URDLDDDRRUULULDDDRDLULDRRRUULULDUUULLDLDLDDDRRLLUUURURRRDDDLDURRDDLRUULLDDUURDLLDLDRUURDDDDRRDU
LDDDDDDDDDDDULLLLDUR UUULDRUUULUUUUDDDDRDDLUUUURLRUUULRDDDDDDLDDDDRRRRURRDDRRRLLLUUUULLUUUUUUUL
UUUUURUUUULUUUUUUUUURRRRRRRRRRRRRDDDLLLDLURRRRUULDRDLL LDLURRRUUULLDURRDLDRDLLDLLUDRRULDLURRRUU
ULLLDURRRDLLLLLLDDLLLRRRUUURRRRRRDDDDRUURRRRUURRRRDDDDLLLDDRDDDLLULLDDDDDUURR
"""

def cheat8 = """
DULLDDULUUUDRRUDLLDDLLDDLLLUUUUURLLLRDDLLLLUDRUUDDDRDDLLDDRDDDDDDRRDRRRRRRUUUUURRUULRDLDLLLUURR
LLDDDRRURRUURRDDDDDUUUUULLDDDDLLD DDRRRRLLLLLLLLLUUURUULUURDDDDLLLLUURRRRDDLLLLDUDRRURRUULLLxDD
URDDDLRRRRRRRRRRRRUDLLUUUUURRDLDLLUDRDDRRRUUDDLLUUUDDDLxLDDDDDDDRDR DDxRRRUULLLUULUUUUUUURURRDL
LLDDDDDDULLLLLLLLLULLDDDDDDUUUUUURRDLLDDDDDDDLLLLLLUUUUUUUUUUUURRRDDDDUULRRUULDURRRRDDLRURRLDUL
ULUxDR RRRRUUUURRRRRLLLLLDDRRRRRDDLRDDDDLLLUUUUDDDDDLLLLLLRUURRRULDLLDDRRRRRDDDLDRRRUUURLDDDLLL
LLLUURLLLLLUUUURLDDLLDDDDRRRRLLLLLxURUUR RRURRxLLUUULULDRRDDDLUxDxULLDDUUUUURRRRLLLLLLRULLDRRLL
LDDDDRRUUDDLLUULLRRDDDLRURRDLLUUULLDULDURRRDLLLLRRRRUUULLULDLRLDDDDLLDUUUU UDDDDRRUURRRRUURRRRR
RRRLLDDDDRRDRRRLRRRRLLLLUUUURRDDRRRRRLDDRRRUURUUURRRUUUUUULLLLLUUDDDLDLLLLUUUULLUUUDDULLLLLLLLL
LLDDDDDDDLLLL LLUUUUURLRDDRxULLDLLLDDLDDLLLLLLDDDDDDDDDDDURRRLLLUUUUUUUUUURRRRRRRDRRLLULUURUURR
RUUDDURRRRDDDLDDRLLLDDUUUULLxLUUULUUUUUURRRRRRU URUURRRURLUURLUUDDRRRRUUxDxLLLLDDDDRRRRDDDDDLLL
xRRRRDDDUUUURUUULLLLUULLDLLLDDLDDLLLLUUUURDRDUUDLLDDDRRRRUURUULLRLRLRUUUDLLLLURDD
"""


def cheat9 = """
UURRRRLLLLLURDRRUUDDDDRRRRDRUDURRDDDLDLLDLLLULLDDLLDURRRURRUUULRDDDLLLDDDDRRDDLLLLUUDDRRUUUUUUU
ULRDDDDDDDDDRURRDULLDDRRUULLLUUUU URRDRLULLDDDDDDLLDDDDDRUULUUURRRRRDRUUUULLLDDDRRRRDDDRDLLULLR
RDDRRRRRRRRRRDDDDLLLLLLDDDLLULLUULRRDULLDDDLLLRRRURRRDRRRUUURRRRRRR UURRRDDRLUULLLDDDDRRRDRULRU
UUURRDDUURRRRRDDDLLDDUDLLULDRRLURRRRUUUULLLLLLLLLLLDDLxRRRLULRLUUDDULRDRDDDRLUURDDLUUDDLRUULLLD
DLLLUR LRUUUULLDDLLLRRLRRUULLDLRUUULLLLRDDUULLDDRDLDDRDULLLDDRDDDRURRDLLLLLLDDRRRRRRRRRLLLLLLLL
RUURUUULULRUDDRDDDRRRURLRULRUULDDLLLLLRR RRDRDxLLLLLUDURRDLLLLDDDRRRUDRULRUURRRRDLLDUUUUUxLLLLL
LLDDRRRRRRRRRRRUURRURRRRLDLLLRURRRRDxURRUxLLLLLLRRRRRRRDDLUDLLRLLRRUURRDRR URLULLLLLLLLDLLLUUUD
RUUDLULLLLULUULLLUxULUUUULLLULLUUUULLRRDDLUULLLDLRURRRRUUUUUUUURRLRDRRURRRDLLLLDLLLUURRDRDDLUUU
LLUxURRRUUUUU xLUDLLRUULRDDRxRRUUULxRRRDDDLDRRDDRRLLUUUURRDDRLUURRRRDDLLxRDDRURRRRLDRUUDRRDDxUR
RLLDDDUUUURRRUDDDDDDDLLRRUUUUUULLLUURURURRRRRRD DLLxUULLUURUURRRRRRRDDDDLUUDDRRRUUUDxRRDxLLURRD
LLRRDDLLDLLLURUULDDDRDUxDLLRRDDxRRRDDDRLLLRRUUUULLLLDDDDUUUURRUUUULLLLLDLDLLDLLUU UDDUDDRRUULUD
RRRDLDDDRRRDDDDDRRRRRRDD
"""

def cheat10orig = """
DDLLDLDDRURLUUUULLDDLRUURRDDDDDLLLLLUUUUUDDDDDLLLLUUULRRDRUUDDLLUUULLLLDDDDDDDDDDDDRRUUUUURRRDU
RRLLLDDDRDDRRRRRRRRRRRRRRRRRRRRUU ULLDLRUULLLULRDLDLLLLRRURRDLLLLRRRURRRRRRRDDDDLLLLLLLUUULLLLL
LLLLDLLRURRRUULRDDDRRUUDRRUUDRRRRRRRRRRDDDDRRUUUUUUUUUUUUUUUUUULLDL LDDRRLLUURRURRDLLDULLDDDDDU
UULRRUULLRRRRRDDLLLLDDDLDRRDDDLDLURRRDRRUUUUUUUUUUUUUUULLLLLDDUDUURDURDDDUUULLLLLDLLLLULLLRDDDD
DDDRDL LLUUULLLDRULLxLLDDDUDDLLLDURRRRRDULLUUUULLRRDDLLUDLUULLLLLLLLLLLLLLLLLLLLLLLLDDRDDLDRRRR
UUULDLRURRRDDURDDRLUUUUDRRRRDDUDLLRDUURR RDDLRUUULLLLLLUURRRRRRRRRRRDDDDDRURDUDRRLLLLUUUUULLLLL
LLLLLLLLLLLLLUUUUUUUUUUUUUUUUUUUUUURRRDURRDDDDLLLLULURDDRRRRRLUUUURURRRRLL LLDLDLUULLLLLUURRRRR
RRRRDRRRRURRDDDDDDDDLLLLLUDRRRRRRLUUURxRRRRRURDDRRRRUULxUURRRLLLDDDRRRLLLDDRUURRRDDDDDDDDR
"""

// I think the original has the keys missing whenever you enter the beamMeUp. This adds them back in!
def cheat10 = cheat10orig.replaceAll( " ", "" ).replaceAll( "UUURRRDURRDD", "UUURRRDURRDDL" ).replaceAll( "RRRRLLLLDLDL", "RRRRLLLLDLDLD" )

def cheat11 = """
x
DDRRRRRUUUULDRDLRDDLLLLUUDDRRRRUUUULLLLLRLDDDRDRRRUUDLRLUUUULLLUUURURRRLLULRLDRRRRDDRRDDRRRURRD
RRRDDRLLRUULLDxLLDRRRUURRDDUULLLL UDRRRUULLLRRRDDLUULLLLLDDDDLDLLLLLLLLLLLLLLLULUUDDLUUUULLLLDD
DDDRDDDDDDDDDRDRRDDDRRRRURUUULRDDDLURUULDLUDRRRRULDDLLDDLURUUUUURLD DDDLLLLLLLRUUUUULUUULUUUUUU
UURRRRDDDDDLRURRDRRRRUUURRURLLLLDLDURUULDLRRRRRRDDURRDURRLUULRDRRURDDDRUUURRRRRRRRRDDDDDLRDDDDD
DDDDDU LLLLDDDLLDDDUUURRUUURRRRDDDLLLLLRUUURRRDLLLDRDLLLUULLLDDDDUUUUULLDDRLRRRDUDDUUURRDDLUULL
LLDDDLLLLLLLUDRRDDDUUURRRUUDRRDLLLRRRUUU RRRDDDDDLLLUDUULLLDLRLLDDLLLURDRRUUURRDDDUUULLDDUUUDRR
DDDDURDDLLLLLLDRRRURRDDxURRURUULLLUURLDDRRRDDDRRRDRRRDDDDLLLLURRRURxURRRRD RRRDRRRRRRDUUUUUDDDD
LxURRRRxRUUUxRxUUUDDDDDDDDDRLLLLRUUULLLLULLLLRLDxLLLLLULLLLRUUURRDDRLxUULLLLLLLLLLULUURLUDDLLLL
LUDRUDLxDLLLU LLLLLLLLLULLULLLLULULLDRRxRRRRRDDDDDDDRRRRLUUDLUULLUUURRRRRRRRRUUULUUURRRUURRRRRD
DRRUUULULUUDRRUUULLLLLLLLLLLLLLLLLLLDLLUxDRRRRR RDRRRRRRRRUUULUUURRRURRRRRDRRRUUULLUUUUULLLLLLL
LLRxRxRRRRRRRRDDDLUUUULLLLLLLDDULDDULDDRLLLUUURRDDUULDDDDDRURRRURRURDRLLLLDLLDUUU URURDUULDLxRR
RRRDUURDDDRRRRRURURRRRRRRRRRDDDLLLDDULxRRRRRRRRUxURUUULLLLLLLDLLRUULRLRDDDLxLLLLLLLLDRDxLLLLURx
LDLLLLRRLLUUURRxDRUR RUUxLDDDRRxLDRDxLLLUxLDLLxUURxURRUUxLDDDRRxLDRDxLLULxLDLLxUURxURRUUxLDDRDR
xLDRDxLULxLUxULLLLLxLxURRLDRRRRDxRLLLULLDDxULLLxLLDDLD UUUUURRRRRRRRRRRRRRRRRRRRDDDDDDRRDDDRRLL
LLLDDDRRDDDDLRR
"""

def cheat12orig = """
x
DLLLLLLRRRRRRUURRRRDRRRRUUUURRUUDLRULULLLRRRDDLDURDDLLDDDDLLLUURLUURLURUUUUUURRRRUUUULLDUUURRU
DLLLUDRRUULRDDLLLLUULLLLLDDDLLLUU URDDDLLLDDRLLLLLLLUUUUURRRDLLURRRRRDLLLLLLDLLLRRRURRRDRURRDDR
RRUUULDDDLLDDLLLLLLUUUUURRRRRRDDDDDLLDDUULLLLLLULLLUUUUDDDDLLUDDULR UULUURRLLDDRDDRRRRRDDDRRRUR
URDRRDLDLUUULLLRDDDDDDRRRRRRUULUULLDDULUUULLDDDDDDRRRRRUUUULLDLUUULLDULLLULLLLLUULURRLDDDRRRRRD
RRRDDL LLLLLLLLDDDDRRLLUUUURRRRRRRRRUURRRLLLULUUURRRRRRDRRRDRRRRULLUURRRRRDDRRRDDRRDDDDLLLLDDDD
DDLLLLLLLDDLUUDDDDRRRRDDUxLUURRRRRRUURRR ULUUUURRRRRRDDDRRRRRRUUURRRRDDRRRDDRRDDDDLLLLDDDDDDDDL
DDLLLLURRLLDLLLUUUURRRRRRRDDDDLLLRRRUUUULLLLLLLDDDDRRDxULLxLxLRRRRRRUUUULL LLLDDxURRRRRRUUUULLL
DLLxLLDDLLLRRRRRRRRRRUUUURUUUUUURRRRUUUULLUULLLUULLLLLDDDLLLLLDDLLDDDURRDDRURUDRRRUUxRRURLURDRR
RUUUDDDLLLUUU RLDDDRRRUUUUUUULLDLRUUDRURLRDRDRRRLDDURDDLUUDLRRUUUUUULLUULLLUULLLLLDDDLLLLLDDLLD
DRRDDRURRRUULRDRRLLDLRRLLDDDxDRURLLDDLRDUDDUUxU RUULLLLDDDLLDDLUULLLDLDURUURDRRDDLxLLLLLDDDLLLD
DDLDURRURRRDDDLLLDLLDDRLLRRDDDRDDLxRRRRLRRDDDLLLDLLLLDRDDRRDLDxUUUULLxUUUULLxUUUx UUULLLULLLLDL
DDDRDDLRRLDDLRUULDURRRDDRRLLLLLDDDDDDDRRRURDUUULLLLUUUUUURUUULUURURRRRDDDDLLUUDDRRDDUUURUURRDDD
RDDRRDDDDRRRLDDDLLUD DLLULULURUULRULUUDDRDDDDDRDRRURRUUURRRDDDRRDDLLLLRRRRUURRUURRUURURRDRDDRRR
DDDDLULDLLLUURRxDDDRDDRRDRRDRRDDDRRRRUURRUURURRDRDRDRR LDLLRRRDDDLLLLLUUDxLRURLDDUURRDDDLLUUUUD
LDLLLRRRRUUUULULLDLLDDLDDLLLLUUULLLLULUUUULUURRURRRUUULLLLUULLLDUUUUULLLLLLLRRRRRUUULLLU UDDUUR
RRRRLLLLDLLLURRRRRRLUURRDDDDRDDDLLLLLLLLRRRRRRRRDDDUUULLLLLLLLRLxULLRRRRUUUULULLDLLDDLDDLLLLUUU
LLLLLUUUUULUURRURRRUUULLLLU UUDRRRUUUULLLUULLULUDRDLUUULLLLDDRRDRRRDDLLLLRRRRURRRRDRDDDDLLLUUUU
UULLLLLRRRDRRRRDRDRRRRDDLUULLDLUULLDLUUULLDDLLLLLLDD
"""

def cheat12 = cheat12orig.replaceAll( " ", "" ).replaceAll( "UURRUUDLRUL", "UURRUUDLRULL" ).replaceAll( "RRRDDLDUR", "RRRDDLDUUR" )

def cheat13 = """
x
LLUUURRDULLLUDRRUUULLLLDDDRLUUURRRURRUUUULLLRRDDDDLLLLUUUUUUUURRRRRDDDUULDUULLUUUUUUULUUULLLDRR
LRDUULLULRRRRRLDDRRLURRRULLLLDDDL DRRDDDRRUDLLUUURRRDDDDLULxLLDDRRUURRRDLRDLRDDLLLUDLLUUUDDDRRR
RUUUULLLLUUUURRDDRRRUUUULLLURDULUURRRRRRRRRRRRRRRRDRDRDDURDULLUULLD LLLDDRDDDLRRRRRDUUUUDDDLLLL
URRLUUDDRUUUDLLLUURRRUURDRDLLLLRRDDLLLLLUULLLDRRLDDDRRDDDLLDRRDDDRRRURRRRDDLDLDDDDUUUURDULDDURD
DDUULL RRRDDDDLLLLRRRRUULLDDLLUUUURRDDDDLLLLLULLLLDRUUULRLLDDDDDUUURRRLLULUURULRRRRRRUDLDDDDDRR
RRLUURULDDDRRRLUUURUUUUUULLDLLUDRRRRDDDD LDRUUUUULLLUUURLLUDRDDDRRRDDDLDDRRDDRRRRRRLUUUUUUUUUUR
RUUUDDDRRDDDDDDDRDDUULLRRDDLLLLDRURxLDDDRRDDDDDDDDDDRDDUULDULDRDDLDLxDDDDD URRDDDLLLxRDUUUUUULL
DRRLLLLUUUUUUUURURRRRRDDDDDDDDDDDDLLUUUUURLDDDDDDDDDRDUUULUULLLUURUUURRRRUUUUUUULLLLLDDDDDDDLDD
LLUUUUUUUUUUL RRUUUULRDDRRxRRULUULLLLULUULUUUUUUUUUURUUDxLLxDLLRDDDDDDDDDDLLLUUUUUUUULLLDLLLUUU
UUUUUUUDRRUURRRUULLLLLLLLLLLLLLLLLDDDDDDDDDDDDD UURRRLDDDRRUUxDDLLRDDDDDDDDDDLLLUUUUUUUUUDLLDLL
LLUUUUUUUUURRRRRUUUULLLLLLLLLLLLLLLLLDDDDDDDDDDDDDRRUURRRDDDRRRRURRLDURRRRRUULDDL LLLULDDLLLxDD
LLLLDDRDDDDDULDRRLUUUUURRRRDLLULLDDDDRRUDLLURRURUULDLLDDDDLDUURUUULDDDDDDDDDRDRRURDUULLLRLDDDDL
RRRRRRRRRRRRRRRRRRRR RRRRUULLLLLLLLLUUULDDRLDRRRLUUUURDDRRRUULDRRDULLDRDDLLLRRULLLLLLLULLLLDDLR
UULRDDLUUUUUUUUUUUUDRRRRRRLDDDDDRRUUURRRRRUULUUDLUULLL LLLLULDDLLUUUUUDDDDDDURRRRURRRRRRRRDLDDD
DDDRRRRRRDDDRRRDDDDRDDL
"""

def cheat14orig = """
DUxRRUUUUUDDDRRURRUDRRLUDLLURUUUDRLDDDRRRURUUUUULLLLUUULUUUUULLULLLDRURRRDLLLRDDUUULLLUULUULUDR
RLLLLDLDDDLLLLRRRxRDRDDDRRURRRRRU RUUUUULLLLUULLURUUUUULLLLLLUUULULLLxRUUDRLDRRDDDUUULURLRUURRD
RRDDDDDUURRRRRRLUURRRRRDDDDDRRDLRDDLxRRRDDRRRDRRRRDRLUURRRRRDDDDDxU UDLRULLLDURRRRxULLRRURRUUUU
UUULRUULLRRDDDDDDDDLLUULLLLLRULRLDDDDDDDDRDDxLLUUUUUULLLLLDDDDDDDLDRRRDRDRRRLDDDLRDDLLLUUULLLUU
xDRRRL UxRRRRRLLLUUUUUUUULUURRRRRxLLUUUUUUULUURRUURUUUUDDDRLxLLLLLDDDDDDDLDRRDLDRRRRDDDDRRRLRUU
DDDDDDLDDLDDUULDUUDURDDDDLLLUUDUDDRRRUUU LLRRDDDLUUUULLUDDRRRRDDDRLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
UDRRRRUUULxDDDRRDDDDUUUULLLLLDDDDDDRDDDDRRRRDDDDRRDDDDLDDDDDLLLLLLLLLLLLLL LLLLULLDLLLLLRRUUDLU
RUDRUUDDLLUxLLRxDDDRRRRLLLLUUURxRLLLUUUUUUUUUDRLUUUUUUUUUUxLLLLLUUUUUUUUUUUUUUURRRUDDDUUURUUDLU
ULRxDDUUUURLU UUUUURRDDDDRRRRURLRDRDULLLLLLUUUURRRRRRRRRRLDDURURRRRRRDRDDDUDRLURLRURURRDULLULxD
DDDDULLDDLDDDDDDDDDDDDDRRRRRDDDRRRRRRRRRRRRRRRR RRRRRRRUUUUURUUUULLLDDLLLLLDDRDDDLLLLLLLLLLUUUU
UUUUUUUUDDDDDDDDDDDDUUURRRUDUULLLUUUUDRRRUUURRRURUULRDDLLLLLUUURRRRRDDDLLLLDDDDLL LDDLLULLLDDDR
DRRLUUUDLLLURURRLLUURRDULLDDUULLDRDDURRDDLRDLLLRRDRLUULRULLRRDDDLLLLURLDRUURLDDRRDRRRUDLLUUUUUU
LDLDDLDDDDDDUULLDD
"""

def cheat14 =  cheat14orig.replaceAll( " ", "" ).replaceAll( "DRRLUUUDLLL", "DRRLUUUDLLLL" ).replaceAll( "DRDDURRDDLRDL", "DRDDURRDDLRDLL" )
    .replaceAll( "RULLRRDD", "RULLRRDDR").replaceAll( "RUDLLUU", "RUDLLUUUR" )


def cheat15orig = """
URRRDDxLxLxUURRRDLULLLDDxLxUURRRDLULLLDDxLxUURRRDDULULLLDDxLxUURRRDLULLLDDxLxUURRRDLULLDDxULLLD
DDDRRRRRRRRLLLLLLLLLLUULLULUURRRD DDLLUxLDDDLLUUUULLRRDDLLLxLxLxLxLxLxLxLxLRDDLLLLLLUUUDRRxDDDL
LLLLUDLxLDDULLLLUUUURRRDDDRDLUULDRUUULLLDDLxUULDLDLLUxRUURRRDDDDLLD LURRRUUUULLLDDLxRUUDDDLxDRD
RRRUUUULDDDDLDLUULUxRRURUUUULLDDDLLDRxLDDDDDRRUURUULRLxLDDDRxLULDRRRDDLULLUxRURUUUULLLDRRRDDDLU
UUULLD DDDDDDDDDDDDDDRDLRDDRDDRRUUxDDDDDDDDDDDDDDDDRRRRUULLxDLLUURRDDLDRUURL...DDLLUUULLUUUUUUUUUU
xLLUUUUURRRUDRRDLRDDLLRxDDRDDUURRDDDDLLL DDDDDDDUURRDDRRRRRDDUUUUUUUUUDRRRRRRRRDDDDDDUURURRRRRR
UUURRRRRRRUUUUUUUUUUUUUULLLLLLLLLLLLLLLLLLLLLLRRRRRRRRRRRRRRRRRRRRRRDDDDDD DLLLLLLLLLLLLLLLLLLL
LLLDDDDDDDRRRRRRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLLLLLLDDDDDDDRRRRRRRRRRRRRRRRRRRRRRLLLLLLLLLLLLLL
LLLLLLLLULLLL LUULLUUUUUUURUULUUUUUUUURRDRRRRDRRRRRRRRR
"""


def cheat15 = """
URRRDDxLxLxUURRRDLULLLDDxLxUURRRDLULLLDDxLxUURRRDDULULLLDDxLxUURRRDLULLLDDxLxUURRRDLULLDDxULLLD
DDDRRRRRRRRLLLLLLLLLLUULLULUURRRD DDLLUxLDDDLLUUUULLRRDDLLLxLxLxLxLxLxLxLxLRDDLLLLLLUUUDRRx
DDDLLLLLUDLx
LDDULLLLUUUURRRDDDRDLUULDRUUULLLDDLx
UULDLDLLUxRUURRRDDDDLLD LURRRUUUULLLDDLx
RUUDDDLx
DRDRRRUUUULDDDDLDLUULUx
RRURUUUULLDDDLLD.L.Rx
LDD.L.DDDRRUURUULRLx
LDDDRx
LULDRRRDDLULL.L.Ux
RURUUUULLLDRRRDDDLUUUULLDD.LL.DDDDDDDDDDDDDRDLRDDRDDRRUUx
D.L.DDDDDDDDDDDDDDDRRRRUULLx
DLLUURRDDLDRUURLDDLLUUULLUUUUUUUUUUx
LLUUUUURRRUDRRDLRDDLLRxDDRDDUURRDDDDLLL DDDDDDDUURRDDRRRRRDDUUUUUUUUUDRRRRRRRRDDDDDDUURURRRRRR
UUURRRRRRRUUUUUUUUUUUUUULLLLLLLLLLLLLLLLLLLLLLRRRRRRRRRRRRRRRRRRRRRRDDDDDD DLLLLLLLLLLLLLLLLLLL
LLLDDDDDDDRRRRRRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLLLLLLDDDDDDDRRRRRRRRRRRRRRRRRRRRRRLLLLLLLLLLLLLL
LLLLLLLLULLLL LUULLUUUUUUURUULUUUUUUU.R.URRDRRRRDRRRRRRRRR
"""

save( "01", "02", "Dots and Waves", "brick1", level1, cheat1 )
save( "02", "03", "Something Fishy", "brick2", level2, cheat2 )
save( "03", "04", "Chicken Supreme", "brick3", level3, cheat3 )
save( "04", "05", "Explosive Mixture", "brick4", level4, cheat4 )
save( "05", "06", "Henry's Anguish", "brick1", level5, cheat5 )
save( "06", "07", "The Dolls House", "crazy1", level6, cheat6 )
save( "07", "08", "Dolly's Revenge", "brick2", level7, cheat7 )
save( "08", "09", "Enlightenment", "crazy2", level8, cheat8 ) 
save( "09", "10", "The Challenge", "brick3", level9, cheat9 )
save( "10", "11", "Patience Pending", "stone1", level10, cheat10 )
save( "11", "12", "Razor Edge", "crazy1", level11, cheat11 )
save( "12", "13", "The Happy Hour", "crazy2", level12, cheat12 )
save( "13", "14", "Deja Vu", "brick4", level13, cheat13 )
save( "14", "15", "Penultimate", "brick2", level14, cheat14 )
save( "15", "pass", "The Decoder", "brick1", level15, cheat15 )

"Done ${new Date()}"
