import uk.co.nickthecoder.tickle.Game
// Reset all "completed" preferences

def preferences = Game.instance.preferences.node( "completed" )
preferences.clear()
