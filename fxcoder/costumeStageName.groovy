// Updates the default stage name for most costumes.
// Most are set to "main".
// A few non-items, such as explosion, speech etc are not set.
// 'test' is set to 'extra'.

// NOTE. It is best not to have any Costume tabs open when running this script.
// Save the resources after you run the script.

import uk.co.nickthecoder.tickle.resources.*
import uk.co.nickthecoder.tickle.Costume

def extras = ["test"]
def none = ["explosion", "speech", "speechText"]

public void updateStageName( String costumeName, Costume costume, String stageName ) {
    if ( costume.stageName != stageName ) {
        costume.stageName = stageName
        println( "Updated costume '${costumeName}' to stage '${stageName}'" )
    }
}

for ( entry in Resources.instance.costumes.items ) {
    def name = entry.key
    def costume = entry.value

    if (extras.contains(name)) {
        updateStageName( name, costume, "extra" )
    } else if (!none.contains(name)) {
        updateStageName( name, costume, "main" )
    }
}
