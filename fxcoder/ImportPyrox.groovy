// Imports scenes from Pyrox.
// NOTE. Only the basic layout is imported.
// Role Attributes, are NOT imported, and must be added manually.

import java.util.regex.*

void importPyrox( String srcName, String destName ) {

    File src = new File( "/home/nick/projects/itchy/resources/Pyrox/scenes/" )
    File dest = new File( "/home/nick/projects/burton/scenes/" )

    def mapping = [
        "balloon" : "balloon",
        "rock"    : "rock",
        "carR"    : "carE",
        "carL"    : "carW",
        "coin"    : "coin",
        "soil"    : "soil",
        "safe"    : "safe",
        "key"     : "key",
        "gate"    : "gateCompleted",
        "warp"    : "gateLocal",
        "player"  : "burton",
        "wall"    : "wall",
        "wallSE"  : "wallSE",
        "wallNE"  : "wallNE",
        "wallNW"  : "wallNW",
        "wallSW"  : "wallSW",
        "stamp"   : "stamp",
        "sledge"  : "sledge",
        "hint"    : "info",
        "table"   : "table",
        "chair"   : "chair",
        "autoPilot" : "controller",
        "grenade" : "grenade",
        "grenade2": "grenade2",

        "squashE" : "squash_E",
        "squashW" : "squash_W",
        "squashN" : "squash_N",
        "squashS" : "squahs_S",
        "squashEW": "squash_EW",
        "squashNS": "squash_NS",

        "squashEP" : "squash_P_E",
        "squashWP" : "squash_P_W",
        "squashNP" : "squash_P_N",
        "squashSP" : "squahs_P_S",
        "squashEWP": "squash_P_EW",
        "squashNSP": "squash_P_NS"
    ]


    def srcFile = new File( src, "${srcName}.xml" )
    def lines = srcFile.text.split( '\n' )

    def buffer = new StringBuffer()

    buffer.append( """{
  "director": "PlayDirector",
  "background": "#000000",
  "showMouse": true,
  "layout": "default",
  "stages": [
    {
      "name": "main",
      "actors": [
""")

    def pattern = Pattern.compile( ".*?actor costume=\"(.*?)\".*")
    def xPattern = Pattern.compile( ".* x=\"(.*?)\".*" )
    def yPattern = Pattern.compile( ".* y=\"(.*?)\".*" )

    boolean first = true

    for ( String line : lines ) {
        def matcher = pattern.matcher( line )
        def xMatcher = xPattern.matcher( line )
        def yMatcher = yPattern.matcher( line )

        if ( matcher.matches() ) {
            def oldName = matcher.group( 1 )
            def x = 0.0
            def y = 0.0
            if ( xMatcher.matches() ) {
                x = xMatcher.group( 1 )
            }
            if ( yMatcher.matches() ) {
                y = yMatcher.group( 1 )
            }

            if ( mapping.containsKey( oldName ) ) {
                def name = mapping[oldName]

                if (! first ) {
                    buffer.append( ",\n" )
                } else {
                    first = false
                }
                buffer.append( """        { "costume": "${name}", "x": ${x}, "y": ${y} }""" )
            } else {
                System.err.println( "Unknown costume : '${oldName}'" )
            }
        } else {
            // System.err.println( "Unmatched : ${line}" )
        }
    }

    buffer.append( """
      ]
    }
  ]
}
""")

    def destFile = new File( dest, "${destName}.scene" )
    destFile.text = buffer.toString()
}

// These are commented out, because I may want to add extra stuff into the levels,
// and I don't want them being overwritten by accident.
// Therefore, I comment each line after it has been successfully been imported.


//importPyrox( "rocks", "rocks/1a" )
//importPyrox( "rocks1a", "rocks/1b" )
//importPyrox( "rocks1b", "rocks/1c" )
//importPyrox( "rocks1c", "rocks/1d" )
//importPyrox( "rocks1d", "rocks/1e" )
//importPyrox( "rocks2", "rocks/2a" ) // NOTE I have renamed these rocks scenes since importing!
//importPyrox( "rocks3", "rocks/3a" )
//importPyrox( "rocks4", "rocks/4a" )
//importPyrox( "rocks5", "rocks/5a" )
//importPyrox( "rocks6", "rocks/6a" )
//importPyrox( "rocks7", "rocks/7a" )
//importPyrox( "rocks8", "rocks/8a" )


//importPyrox( "balloons", "balloons/1a" )
//importPyrox( "balloons1a", "balloons/1b" )
//importPyrox( "balloons1b", "balloons/1c" )
//importPyrox( "balloons1c", "balloons/1d" )

//importPyrox( "balloons2", "balloons/2a" )
//importPyrox( "balloons2a", "balloons/2b" )

//importPyrox( "balloons3", "balloons/3a" )
//importPyrox( "balloons3a", "balloons/3b" )
//importPyrox( "balloons3b", "balloons/3c" )

//importPyrox( "balloons4", "balloons/4a" )
//importPyrox( "balloons4a", "balloons/4b" )

//importPyrox( "balloons5", "balloons/5a" )


//importPyrox( "furniture", "furniture/1a" )
//importPyrox( "furniture2", "furniture/2a" )
//importPyrox( "furniture3", "furniture/3a" )
//importPyrox( "furniture3a", "furniture/3b" )

//importPyrox( "grenades", "grenades/1a" )
//importPyrox( "grenades2", "grenades/1b" )
//importPyrox( "grenades3", "grenades/1c" )
//importPyrox( "grenades4", "grenades/1d" )
//importPyrox( "grenades5", "grenades/1e" )

//importPyrox( "vehicles", "vehicles/1a" )

// Requires planes, which I haven't coded (yet or not at all?)
//importPyrox( "vehicles2", "vehicles/1b" )
