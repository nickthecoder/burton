# Burton

A retro game, loosely based on the BBC Micro game, Repton.
When I was a kid, I loved the Repton series of games.
Repton Infinity was life changing - it wasn't only a game, it was a game designer,
where I could write the logic for each game character.
Burton follows on from where Repton Infinity left off.

![screenshot](screenshot.png)

FYI, the town of Repton in Derbyshire, England is near Burton upon Trent.

Aim of the Game
---------------

Collect coins, stamps and other collectable items. Don't get hit on the head by rocks,
and don't get trapped.

Sounds easy, but as you progress, things get more and more tricky.
Are you good enough to solve them all?

Starting the Game
-----------------

To play the game, you must first install
[Tickle](https://gitlab.com/nickthecoder/tickle).

Start Tickle, then click "Play", and choose the file : "Burton.tickle".
(If you've set up the proper file associations for ".tickle" files, you can double click it instead).

Status
------

Complete.

There are many level in the spirit of the classic Repton, but also introduces
many more objects.

There's also a single level which mimicks the TRS-80 game _Cavern Quest_.

Plus, it also mimics all 15 levels of the BBC Micro game _XOR_.

So thet's 3 games for the price of none ;-)

There's good coverage of "unit tests" for Burton's standard game objects.
Minimal coverage for the Cavern Quest objects.
The testing for the XOR objects consists of solutions to each of the 15 levels.

History
-------

This is my fourth time writing a grid-based puzzle game.
All have stemmed from the enjoyment and challenge from playing
Cavern Quest on the TRS-80 as well as Repton 3 and XOR on the BBC Micro.

Game Style
----------

Repton and XOR have different styles, even though they look superficially 
very similar. XOR is pure, in the sense that it is all about solving the
puzzles, and there is no need to be nimble.

I personally prefer XOR's style, but adding a little bit of excitement from
being chased by deadly creatures can be good. I never liked the timed aspect
of Repton 3 though.

Cavern Quest is interesting, because you can play it in two ways.
As a child, I often let the enemies out, and then I'd need quick reflexes
and good timing to knock them on the head.

Now, as an adult, I understand the enemy's behaviour, and I can defeat them without reflexes or
timing. I can literally pause between every move, and it would make no different,
just as with XOR.

The standard Burton levels require a little bit of nimbleness, but there is
(currently) no timed elements.


The Future
----------

Then what's next?

Then there's looks of finishing touches, such as sound effects, and better animations.

Powered by [Tickle](https://nickthecoder.co.uk/software/tickle) and [LWJGL](https://www.lwjgl.org/).
